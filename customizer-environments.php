<?php
namespace PRK\CustomizerEnvironments;

//use \PRK\CustomizerEnvironments\Manager;

function customizer_environment_manager() {
	static $manager;
	if ( is_null( $manager ) ) {

		require_once( dirname( __FILE__ ) . '/class-customizer-environments.php' );
		$manager = new Manager\Customizer_Environments();
	}

	return $manager;
}

function register_customizer_environment( $handle, $args ) {
	$manager = customizer_environment_manager();
	$manager->register_environment( $handle, $args );
}


add_action( 'wp_ajax_customize_save', '\PRK\CustomizerEnvironments\customizer_environment_manager' );



//function customize_manager_load(){
	add_filter( 'customize_loaded_components', function( $components ) {

		delight_me( 'yesthisworks', $GLOBALS['wp_actions'] );
		return customize_environment_manager_filter_boot( $components );
	} );
//}

function customize_environment_manager_filter_boot( $components ){

	return customizer_environment_manager()->filter_customize_loaded_component( $components );
}




//add_filter( 'customize_loaded_components', 'customize_environment_manager_filter_boot' );
//