/* global wp, jQuery */
/* exported CustomizerBlankSlate */

var CustomizerEnvironmentManager = (function ( api, $, _ ) {
    'use strict';
    //console.log(spsCustomizeVars);
    var component = {
        data: {
            queryParamName: null,
            queryParamValue: null,
            secondQueryParamName: null,
            secondQueryParamValue: null
        }
    };

    /**
     * Initialize functionality.
     *
     * @param {object} args Args.
     * @param {string} args.queryParamName  Query param name.
     * @param {string} args.queryParamValue Query param value.
     * @returns {void}
     */
    component.init = function init( args ) {
        _.extend( component.data, args );
        if ( !args || !args.queryParamName || !args.queryParamValue ) {
            throw new Error( 'Missing args' );
        }

        api.bind( 'ready', function () {
            component.injectPreviewUrlQueryParam();
        } );


    };

    /**
     * Make sure that all previewed URLs include our special customizer query param.
     *
     * @returns {void}
     */
    component.injectPreviewUrlQueryParam = function injectPreviewUrlQueryParam() {
        var previousValidate = api.previewer.previewUrl.validate;
        api.previewer.previewUrl.validate = function injectQueryParam( url ) {
            var amend = false, queryString, queryParams = {}, urlParser, validatedUrl;
            validatedUrl = previousValidate.call( this, url );

            // Parse the query params.
            urlParser = document.createElement( 'a' );
            urlParser.href = validatedUrl;
            queryString = urlParser.search.substr( 1 );

            // store all the found query parameters in queryParams object
            _.each( queryString.split( '&' ), function ( pair ) {
                var parts = pair.split( '=', 2 );
                if ( parts[ 0 ] ) {
                    if( _.isUndefined( parts[ 1 ] ) ){
                        queryParams[ decodeURIComponent( parts[ 0 ] ) ] = null;
                    } else {
                        queryParams[ decodeURIComponent( parts[ 0 ] ) ] = decodeURIComponent( parts[ 1 ] );
                    }
                    //queryParams[ decodeURIComponent( parts[ 0 ] ) ] = _.isUndefined( parts[ 1 ] ) ? null : decodeURIComponent( parts[ 1 ] );
                }
            } );

            // Amend the query params if our first param not set.
            if ( component.data.queryParamValue !== queryParams[ component.data.queryParamName ] ) {
                queryParams[ component.data.queryParamName ] = component.data.queryParamValue;
                amend = true;
            }
            // Amend the query params if second param is desired and not present.
            if ( component.data.secondQueryParamName
                && component.data.secondQueryParamName !== queryParams[ component.data.secondQueryParamName ] ) {
                queryParams[ component.data.secondQueryParamName ] = component.data.secondQueryParamValue;
                amend = true;
            }

            if( amend ){
                urlParser.search = $.param( queryParams );
                validatedUrl = urlParser.href;
            }
            return validatedUrl;
        };
    };

    return component;

}( wp.customize, jQuery, _ ) );