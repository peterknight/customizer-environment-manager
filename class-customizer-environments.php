<?php

namespace PRK\CustomizerEnvironments\Manager;

/**
 * Edit Post Preview class.
 *
 * @package Single Page Success
 * @subpackage Customize
 */


class Customizer_Environments {

	const QUERY_PARAM = 'CENV'; // user interface set name = dedicated set of customizer controls
	const SECOND_QUERY_PARAM = 'CUIID'; // user interface ID
	const CUSTOMIZE_JS_SCRIPT_HANDLE = 'customizer-environments';

	public $registered_environments = array();
	public $post_id = false;
	public $environment = false;
	public $env_id = false;
	public $env_class = false;
	public $focus = false;

	function __construct() {
		/*
		 * Dev notes:
		 * - We want to adjust the default customizer environment and alter behaviour
		 * - WP_Customize_Manager is instantiated before setup_theme hook, which runs very early
		 * - our init() should load before wp_loaded (customize_register fires then)
		 * - but after post_types are registered (which happens on init hook)
		 * so our init loads very late on the init action hook...
		 */
		/*if( did_action( 'init' ) ) {
			$this->init();
		} else {
			add_action( 'init', array( $this, 'init' ), 999999999999 );
		}*/
		$this->init();
		/*add_filter( 'customize_loaded_components', array( $this, 'filter_customize_loaded_component' ) );
		add_action( 'customize_controls_init', array( $this, 'set_query_param' ) );
		add_action( 'customize_preview_init', array( $this, 'make_auto_draft_status_previewable' ) );*/
	}

	function init() {
		/*global $wp_customize;

		if( empty( $wp_customize ) || is_wp_error( $wp_customize ) ){
			return;
		}*/

		if( $this->is_saving_settings() ){
			$this->ajax_init();

			return;
		}

		//add_filter( 'customize_loaded_components', array( $this, 'filter_customize_loaded_component' ) );
		add_action( 'customize_controls_init', array( $this, 'set_query_param' ) );
		add_action( 'customize_preview_init', array( $this, 'make_auto_draft_status_previewable' ) );

		$env = $this->customizer_environment();
		if ( ! $env ) {
			return;
		}
		$this->environment = $env;
		$this->env_id = $this->set_id();
		add_action('init', array( $this, 'boot_environment' ), 999999 );
		//$this->boot_environment( $env, $this->env_id );
	}

	function doing_custom_environment(){

	}

	function is_saving_settings(){
		return wp_doing_ajax();
	}

	function ajax_init(){
		$this->registrations();
		foreach( $this->registered_environments as $env => $args ){
			if( isset( $args['class'] ) ){
				$env_class = new $args['class']();
				if( method_exists( $env_class, 'ajax_init' ) ){
					$env_class->ajax_init();
				}
				if( method_exists( $env_class, 'ajax_save' ) ) {
					add_action( 'wp_ajax_customize_save', array( $env_class, 'ajax_save' ), 0 );
				}
			}
		}
	}

	function registrations(){
		if( empty( $this->registrations_done ) ) {
			do_action( 'customizer_environment_registrations', $this );
			$this->registrations_done = true;
		}
	}


	function register_environment( $handle, $args ) {
		$this->registered_environments[ $handle ] = $args;
	}

	function boot_environment() {
		global $wp_customize;
		$this->reset_customizer();
		$this->determine_post_id();
		$this->register_default_controls( $wp_customize );

		do_action(
			'customizer_set_' . $this->environment,
				$wp_customize, $this->env_id, $this->post_id, $this->focus
		);
	}

	function id_from_url() {
		return url_to_postid( esc_url_raw( wp_unslash( $_SERVER['REQUEST_URI'] ) ) );
	}

	function register_assets() {
		wp_register_script(
			self::CUSTOMIZE_JS_SCRIPT_HANDLE,
			plugins_url( "/customizer-environments.js", __FILE__ ),
			array( 'jquery', 'customize-controls' ),
			'1.0'
		);
	}

	function set_query_param() {
		global $wp_customize;
		$args = array(
			self::QUERY_PARAM => $this->environment
		);
		if( $this->env_id ){
			$args[ self::SECOND_QUERY_PARAM ] = $this->env_id;
		} else if( $this->post_id ){
			$args[ self::SECOND_QUERY_PARAM ] = $this->post_id;
		}
		delight_me('set_quer_var_cust_env', $args );
		$wp_customize->set_preview_url(
			add_query_arg(
				$args,
				$wp_customize->get_preview_url()
			)
		);
	}

	function enqueue_customizer_script() {
		wp_enqueue_script( self::CUSTOMIZE_JS_SCRIPT_HANDLE );
		$args = array(
			'queryParamName'        => self::QUERY_PARAM,
			'queryParamValue'       => $this->environment,
			'secondQueryParamName'  => self::SECOND_QUERY_PARAM,
			'secondQueryParamValue' => $this->post_id
		);

		wp_add_inline_script(
			self::CUSTOMIZE_JS_SCRIPT_HANDLE,
			sprintf(
				'CustomizerEnvironmentManager.init( %s );',
				wp_json_encode( $args )
			),
			'after'
		);
	}

	static function customizer_post_link( $post, $params, $args = array() ) {
		$permalink = wp_customize_url();

		$url = self::get_preview_post_link( $post, $args );

		$args = array();

		if ( ! empty( $params['environment'] ) ) {
			$args[ self::QUERY_PARAM ] = $params['environment'];
		}
		if ( ! empty( $params['section'] ) ) {
			$args['autofocus[section]'] = $params['section'];
		}
		if ( ! empty( $params['id'] ) ) {
			$args[ self::SECOND_QUERY_PARAM ] = $params['id'];
		}

		$args['url'] = urlencode( $url );

		$permalink = add_query_arg( $args, $permalink );

		return $permalink;
	}

	/**
	 * Generate a preview permalink for a post/page.
	 *
	 * @access public
	 *
	 * @param WP_Post $post The post in question.
	 *
	 * @param array $args
	 *
	 * @return string Edit post link.
	 */
	public static function get_preview_post_link( $post, $args = array() ) {
		$permalink = '';
		$ex = post_type_exists( $post->post_type );
		$inst = get_class( $post );
		if ( ! ( $post instanceof \WP_Post ) || ! post_type_exists( $post->post_type ) ) {
			return $permalink;
		}

		$id_param          = ( 'page' === $post->post_type ) ? 'page_id' : 'p';
		$args['preview']   = true;
		$args[ $id_param ] = $post->ID;

		if ( 'page_id' !== $id_param && 'post' !== $post->post_type ) {
			$args['post_type'] = $post->post_type;
		}
		$permalink = get_preview_post_link( $post, $args, home_url( '/' ) );

		if( ! empty( $args ) && is_array( $args ) ){
			$permalink = add_query_arg( $args, $permalink );
		}

		return $permalink;
	}

	function customizer_environment( $env = false ) {
		// register custom customizer environments
		$this->registrations();

		if ( ! isset( $_GET[ self::QUERY_PARAM ] ) ) {
			return false;
		}
		$current_environment = $_GET[ self::QUERY_PARAM ];

		if ( ! isset( $this->registered_environments[ $current_environment ] ) ) {
			return false;
		}
		$this->environment_init( $current_environment );

		if ( $env && $current_environment !== $env ) {
			return false;
		}

		return $current_environment;
	}

	function environment_init( $env ){
		if( isset( $this->registered_environments[ $env ]['class'] ) && empty( $this->env_class ) ){
			$class = $this->registered_environments[ $env ]['class'];
			$this->env_class = new $class();
		}
	}

	function set_id() {
		if ( ! isset( $_GET[ self::SECOND_QUERY_PARAM ] ) ) {
			return false;
		}
		$set_id = (int) $_GET[ self::SECOND_QUERY_PARAM ];

		return $set_id;
	}


	function post_id() {
		if ( empty( $this->post_id ) ) {
			$this->determine_post_id();
		}

		return $this->post_id;
	}

	function post() {
		if ( empty( $this->post ) ) {
			$this->post = get_post( $this->post_id() );
		}

		return $this->post;
	}



	function determine_post_id() {
		if ( isset( $_GET['url'] ) ) {
			$url     = urldecode( $_GET['url'] );
			$post_id = url_to_postid( $url );
			if ( $post_id ) {
				$this->post_id = $post_id;
			}
		} else if ( isset( $_GET['preview'] )
		            && isset( $_GET['p'] )
		) {
			$this->post_id = (int) $_GET['p'];
		} else {
			$post = $this->get_previewed_post();
			if ( ! is_null( $post ) ) {
				$this->post_id = $post->ID;
			} else {
				$this->post_id = $this->id_from_url();
			}

		}
	}

	function reset_customizer() {
		remove_all_actions( 'customize_register' );
		//add_filter( 'customize_loaded_components', array( $this, 'filter_customize_loaded_component' ) );
		global $wp_customize;

		$this->register_default_controls( $wp_customize );

		do_action( 'customizer_reset' );
	}

	/**
	 * Remove widgets and nav_menus from loaded components if opening in post preview.
	 *
	 * Since all panels and sections are hidden aside from the post type panel and
	 * the section specific to this post, we can save load time by turning off these
	 * components.
	 *
	 * @param array $components Components.
	 *
	 * @return array Components.
	 */
	public function filter_customize_loaded_component( $components ) {
		if( ! $this->customizer_environment() ){
			return $components;
		}
		delight_me('filter_customize_loaded_component', $components, $_REQUEST );
		if ( $this->can_load_customize_post_preview() ) {
			foreach ( array( 'widgets', 'nav_menus' ) as $component ) {
				$i = array_search( $component, $components, true );
				if ( false !== $i ) {
					unset( $components[ $i ] );
				}
			}


			add_action( 'wp_loaded', array( $this, 'remove_customize_actions' ), 1 );

			if( method_exists( $this->env_class, 'filter_customize_loaded_component') ){
				$components = $this->env_class->filter_customize_loaded_component( $components );
			}
		}

		return $components;
	}

	function remove_customize_actions() {
		remove_all_actions( 'customize_register' );
		do_action( 'after_removing_customize_register_actions' );
	}

	/**
	 * Get previewed post.
	 *
	 * @return WP_Post|null
	 */
	public function get_previewed_post() {
		if ( function_exists( 'get_current_screen' ) && get_current_screen() && 'post' === get_current_screen()->base ) {
			$post = get_post();
		} elseif ( is_preview() ) {
			$post = get_post();
		} elseif ( isset( $_GET['previewed_post'] ) && preg_match( '/^\d+$/', wp_unslash( $_GET['previewed_post'] ) ) ) {
			$post = get_post( intval( $_GET['previewed_post'] ) );
		}
		if ( empty( $post ) ) {
			return null;
		}
		$post_type_obj = get_post_type_object( $post->post_type );
		if ( ! $post_type_obj || ! current_user_can( $post_type_obj->cap->edit_post, $post->ID ) ) {
			return null;
		}

		return $post;
	}




	/**
	 * Remove all statically-registered sections and controls.
	 *
	 * The post sections and the controls inside of them will be created dynamically.
	 * Anything construct other than the previewed post's panel should be removed.
	 */
	public function remove_static_controls_and_sections() {
		if ( ! $this->can_load_customize_post_preview() ) {
			return;
		}

		global $wp_customize;
		if ( empty( $wp_customize ) ) {
			return;
		}
		foreach ( $wp_customize->controls() as $control ) {
			$wp_customize->remove_control( $control->id );
		}
		foreach ( $wp_customize->sections() as $section ) {
			$wp_customize->remove_section( $section->id );
		}
	}



	/**
	 * Return whether the Customizer post preview should load.
	 *
	 * There must be a query var for the previewed_post and a valid nonce must be present.
	 */
	public function can_load_customize_post_preview() {
		return true;

		return isset( $_GET['previewed_post'] ) && check_ajax_referer( self::PREVIEW_POST_NONCE_ACTION, self::PREVIEW_POST_NONCE_QUERY_VAR, false );
	}

	/**
	 * Make the auto-draft status protected so that it can be queried. Props iseulde.
	 *
	 * @link https://github.com/iseulde/wp-front-end-editor/blob/bc65aff6a9197aec3a91135e98b033279853ad98/src/class-fee.php#L39-L42
	 */
	public function make_auto_draft_status_previewable() {
		delight_me('customizer_preview_init', is_customize_preview() );
		global $wp_post_statuses;
		$wp_post_statuses['auto-draft']->protected = true;
	}

	function register_default_controls( $wp_customize ) {
		if ( version_compare( strtok( get_bloginfo( 'version' ), '-' ), '4.9', '<' ) ) {
			$wp_customize->register_panel_type( 'WP_Customize_Panel' );
			$wp_customize->register_section_type( 'WP_Customize_Section' );
			$wp_customize->register_section_type( 'WP_Customize_Sidebar_Section' );
			$wp_customize->register_control_type( 'WP_Customize_Color_Control' );
			$wp_customize->register_control_type( 'WP_Customize_Media_Control' );
			$wp_customize->register_control_type( 'WP_Customize_Upload_Control' );
			$wp_customize->register_control_type( 'WP_Customize_Image_Control' );
			$wp_customize->register_control_type( 'WP_Customize_Background_Image_Control' );
			$wp_customize->register_control_type( 'WP_Customize_Cropped_Image_Control' );
			$wp_customize->register_control_type( 'WP_Customize_Site_Icon_Control' );
			$wp_customize->register_control_type( 'WP_Customize_Theme_Control' );
		}
	}

	function js() {

	}
}
