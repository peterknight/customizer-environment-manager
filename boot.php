<?php

/**
 * @category     WordPress Script
 * @package      on-demand-utils
 * @author       PeterRKnight
 * @license      GPL-2.0+
 * @link         https://innovatingwp.com
 *
 * Plugin Name:  On Demand Utils
 * Plugin URI:   //
 * Description:  Load assets on demand and with added configuration options
 * Author:       Peter R Knight
 * Author URI:   http://peterrknight.com
 * Contributors: PeterRKnight (@peterrknight)
 *
 * Version:      1.0.1
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * **********************************************************************
 *
 * On Demand Utils
 *
 * Load assets conditionally and on demand with advanced configuration possibilities.
 *
 * Uses SystemJS and babel while extending the Scripts and Styles API in
 * WordPress with powerful new abilities
 *
 * - load assets (scripts or css) only when you need it (determine such needs clientside)
 * - easily solve and avoid conflicts with other code loading competing scripts
 * - work with ESM modules
 * - let other users easily debug your ESM code without installing any tooling
 * - load assets when certain elements appear on the page
 *
 */

/**
 * On Demand Utils Boot script
 *
 * finds the most recent version of On Demand Utils and requires only that
 */

if( ! class_exists( 'Customizer_Environments_Loader_100') ){
	class Customizer_Environments_Loader_100 {

		const Version = '1.0.0';

		function __construct(){
			add_filter( 'customizer_environments_versions', array( $this, 'register' ) );
		}

		function register( $versions ){
			$path = dirname( __FILE__ ) . '/customizer-environments.php';

			$versions[ self::Version ] = $path;

			return $versions;
		}
	}
	new Customizer_Environments_Loader_100();
}


if( ! function_exists( 'customizer_environments_boot' ) ){
	function customizer_environments_boot(){
		if( defined('CUSTOMIZER_ENVIRONMENTS_VERSION' ) ){
			return;
		}
		$versions = apply_filters(
			'customizer_environments_versions',
			array()
		);

		ksort( $versions );
		$path = end( $versions );

		require_once( $path );

		$version = key( $versions );
		define( 'CUSTOMIZER_ENVIRONMENTS_VERSION', $version );
	}
}